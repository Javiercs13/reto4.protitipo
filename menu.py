def mostrar(self):
    print('------')
    menu = [
        ['1: Anadir contacto'],
        ['2: Listar contactos'],
        ['3: Buscar contacto'],
        ['4: Borrar contacto'],
        ['5: Cerrar agenda']
    ]

    for x in range (6):
        print(menu[x][0])

    try:
        option = int(input('Introduzca un numero'))
        return self.validarOption(option)
    except:
        print('Solo esta permitido el ingreso de un numero de la lista')
        self.mostrar()

def ValidarOption(self, option):
    if option == 5:
        print('Saliendo de la agenda')
        exit()
    if option == 1:
        print('Añade un contacto')